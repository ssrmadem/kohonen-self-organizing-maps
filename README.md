# Kohonen Self Organizing Maps

Implementing Kohonen's Self Organizing Maps from scratch and analysis on the RGB color Dataset on various values of Neighbourhood function (sigma0) and learning rate parameters